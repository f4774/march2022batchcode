// Turn normal class into scheduable class
// so it can help me to run a code perodically without any developer or human intervention
public class AccuntEngagementWeeklyBatch implements Schedulable{

    
    public void execute(System.SchedulableContext SC)
    {
        AccountEngagementBatch oBatch = new AccountEngagementBatch();
		// 2nd paramters deterimes how many records should be processed in each batch
		Database.executeBatch(oBatch,10);
    }
}