import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue} from 'lightning/uiRecordApi';
import { loadScript, loadStyle} from 'lightning/platformResourceLoader';
import LEAFLET from '@salesforce/resourceUrl/leaflet';

import ACC_LAT from '@salesforce/schema/Account.Latitude__c';
import ACC_LON from '@salesforce/schema/Account.Longitude__c';

const fields = [ACC_LAT, ACC_LON];

export default class AccountShippingLocation extends LightningElement {

    @api recordId; // current account's record Id
    @api objectApiName; // current accounts api name
    @api height; 

    @wire(getRecord, {recordId: '$recordId', fields})
    accountRecord;

    @wire(getRecord, {recordId: '$recordId', fields})
    processOutput({data,error})
    {
        if(data) {
            console.log(JSON.stringify(data));
        } 
        else if(error)
        {
            console.log(error.body.message);
        }


    };

    get latitude() { return getFieldValue(this.accountRecord.data, ACC_LAT)};
    get longitude() { return getFieldValue(this.accountRecord.data, ACC_LON)};

    renderedCallback()
    {
        this.template.querySelector('div').style.height = `${this.height}px`;

    }

      
    connectedCallback()
    {
        Promise.all(
            [
                loadStyle(this, LEAFLET + '/leaflet.css'),
                loadScript(this, LEAFLET + '/leaflet.js')
            ]).then(() => {
                this.draw();
                //console.log('this.draw to be called !!!')
            });
    }

    draw()
    {
        let containner = this.template.querySelector('div'); // find the div element 
        let position = [this.latitude, this.longitude]; // assign lat and long t o a array
        let map = L.map(containner, {scrollWheelZoom: false}).setView(position, 11); /**/

        /*L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
        {
            attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
            tileSize: 512,
            maxZoom: 18,
            zoomOffset: -1,
            id: 'mapbox/streets-v11',
            accessToken:"" 
        }).addTo(map);*/


        /*var layer = new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
        map.addLayer(layer);*/

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="<https://www.openstreetmap.org/copyright>">OpenStreetMap</a> contributors',
        }).addTo(map);

        let marker = L.marker(position).addTo(map);
        let featureGroup = L.featureGroup([marker]).addTo(map);
        map.fitBounds(featureGroup.getBounds());

    }
    


}