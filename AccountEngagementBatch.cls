// Turn a normal class into super class(Batch APEX) which could process bulk records
public class AccountEngagementBatch implements Database.Batchable<sObject>{
    
    public Database.QueryLocator start(Database.BatchableContext BC)
    {
        // Collect all the data to be processed
        String query = 'Select Id , Name from Account';         
        return Database.getQueryLocator(query); // returns me list of accounts
    }
    
    public void execute(Database.BatchableContext BC, List<Account> acclst)
    {
        // process each batch of records being sent by start() method
       for(Account acc :acclst)
       {
           System.debug('Acc Name ' + acc.Name); 
           
           // Write code to check which contact has the last activity date 
           //  Update custom fiels with green, amber or red
           
       }
    }
    
     public void finish(Database.BatchableContext BC)
     {
         // I can use this method to send a completion email to admins or developers
         // I can use this method to log any error
     }

}