public class HelperUtil {
    
    //Method
    public void CreateAccount()
    {
        Account objAccount = new Account(); // Step 1: Initalize 
        objAccount.Name = 'Bhaskar Cafe Ltd';
        objAccount.AnnualRevenue = 1000000;
        objAccount.AccountNumber = '1111999911111';
        insert objAccount;
    }
    
    //Method with input parameters
    public void CreateAccountWithParamters(String strAccountName, String strAccountNumber)
    {
        Account objAccount = new Account(); // Step 1: Initalize 
        objAccount.Name = strAccountName;
        objAccount.AccountNumber = strAccountNumber;
        insert objAccount;
    }
    
      //Method with input parameters
    public void CreateAccountWithParamters_V1(String strAccountName, String strAccountNumber)
    {
        Account objAccount = new Account(); // Step 1: Initalize 
        objAccount.Name = strAccountName;
        objAccount.AccountNumber = strAccountNumber;
      
        // Write a code to find if there is already an account with the same name exists and throw error back to the user 
         
        Database.insert(objAccount);
    }
    
    
    public static void CreateContact()
    {
        Contact objCon = new Contact(); // Step 1: Initalize 
        objCon.LastName = 'Mike';
        insert objCon;
        
       /* Decimal dcInput = 3.14, 1.15,5.55 // error out
        List<Decimal> dcInput = new List<Decimal>();
        dcInput.add(3.14);
        dcInput.add(1.15);
        dcInput.add(5.55);
        */
    }
    
    public static void CreateAccountAndContact(String strAccountName, String strAccountNumber, String strContactName)
    {
    
        Account objAccount = new Account(); // Step 1: Initalize 
        objAccount.Name = strAccountName;
        objAccount.AccountNumber = strAccountNumber;
        Database.insert(objAccount);
        
        System.debug('System - debug: You can use me to print something in the log for troubleshooting purpose!!!');
        System.debug('Value of objAccount' + objAccount); 
        System.debug('Value of New Account ID that just got created ' + objAccount.Id);
        System.debug('New account has been created by :: ' + objAccount.CreatedBy);
              
        
        Contact objCon = new Contact(); // Step 1: Initalize 
        objCon.LastName = strContactName;
        objCon.AccountId = objAccount.Id;
        System.debug('Value of objCon' + objCon);         
        insert objCon;
        
        
    }
    
     public static void CreateAccountAndContact_V1( Id AccountId, String strContactName)
    {

        Contact objCon = new Contact(); // Step 1: Initalize 
        objCon.LastName = strContactName;
        objCon.AccountId = AccountId;              
        insert objCon;
    }
    
    
    // LIST , MAP AND SET - Will help you deal with more than one records
    public static void CreateMultipleAccounts(Integer iNoOfAccountsToBeCreated)
    {
        // For loop in APEX
       	for(Integer i=1; i<=iNoOfAccountsToBeCreated; i++)
        {
            Account objAccount = new Account(); // Step 1: Initalize 
            objAccount.Name = 'Account Name - ' + i;
            objAccount.AccountNumber = '11111';      
            Database.insert(objAccount);
        }
        
    } 
    
    public static void CreateMultipleAccounts_V1WithGovernorLimitFix(Integer iNoOfAccountsToBeCreated)
    {
        // Create a collection or a bag to hold accounts
        List<Account> acclst = new List<Account>(); 
        
        // For loop in APEX
       	for(Integer i=1; i<=iNoOfAccountsToBeCreated; i++)
        {
            Account objAccount = new Account(); // Step 1: Initalize 
            objAccount.Name = 'Account Name - ' + i;
            objAccount.AccountNumber = '11111';      
            //Database.insert(objAccount); // NOT A BEST PRATICSE TO CALL DML STATMENT INSIDE FOR LOOP
         	acclst.add(objAccount); // Add new accounts data into the collection
        }
        
        // Issue an insert DML statement and pass account(acclst) collection
        Database.insert(acclst); // This will make single DML call to the database
        
        System.debug('Value of acclst collection:: ' + acclst);
        
        System.debug('Total accounts in the  acclst collection is ' + acclst.size());
        
        
        
    } 
    
    
    public static void ListExample()
    {
        List<String> lstWeekDays = new List<String>();
      	lstWeekDays.add('Monday');
        lstWeekDays.add('Tuesday');
        lstWeekDays.add('Wednesday');
        lstWeekDays.add('Thursday');
        lstWeekDays.add('Friday');
        lstWeekDays.add('Friday');
        lstWeekDays.add('Friday');
        lstWeekDays.add('Friday');
        
        System.debug('Value of lstWeekDays:: ' + lstWeekDays);       
        
    }
    
     public static void ListExample_V1()
    {
        List<Integer> lstEvenNumbers = new List<Integer>();
      	lstEvenNumbers.add(2);
        lstEvenNumbers.add(4);
        lstEvenNumbers.add(6);
		lstEvenNumbers.add(8);
        lstEvenNumbers.add(10);
        //lstEvenNumbers.add(1.34); // This will error out . DO NOT MIX DATA TYPES
        
        System.debug('Value of lstEvenNumbers:: ' + lstEvenNumbers);       
        
    }
    
    public static void ListExample_V2()
    {
        List<Decimal> lstDecimal = new List<Decimal>();
      	lstDecimal.add(3.14);
        
    }
    
     public static void ListExample_V3()
    {
        List<Account> lstAccounts = new List<Account>(); // Hold list of accounts
        
		// Account 1 - Sits at the bottom of the collection - Say index 0
        Account objAccount = new Account(); // Step 1: Initalize 
        objAccount.Name = 'Parle-G';
        objAccount.AccountNumber = '11111';
        lstAccounts.add(objAccount); // Add it to the list
        
        // Account 2  Sits on top of the last item  in  the collection -here, its index 1
        Account objAccount2 = new Account(); // Step 1: Initalize 
        objAccount2.Name = 'Milk Bikis';
        objAccount2.AccountNumber = '22222';
        lstAccounts.add(objAccount2); // Add it to the list
        
                
        // Account 3  Sits on top of the last item  in  the collection -here, its index 2
        Account objAccount3 = new Account(); // Step 1: Initalize 
        objAccount3.Name = 'Marie Gold';
        objAccount3.AccountNumber = '33333';
        lstAccounts.add(objAccount3); // Add it to the list
        
        System.debug('Value of lstAccounts:: ' + lstAccounts);
        
        //Accessing name of the second account
        System.debug('Value is :: ' + lstAccounts[1].Name);
        
        //Accessing account number of the second account
        System.debug('Value is :: ' + lstAccounts[1].AccountNumber);
        
    }
    
    public static void SetExample()
    {
        Set<String> stWeekDays = new Set<String>();
      	stWeekDays.add('Monday');
        stWeekDays.add('Tuesday');
        stWeekDays.add('Wednesday');
        stWeekDays.add('Thursday');
        stWeekDays.add('Friday');
        stWeekDays.add('Friday');
        stWeekDays.add('Friday');
        stWeekDays.add('Friday');
        
        System.debug('Value of stWeekDays:: ' + stWeekDays);       
        
    }
    
    public static void ReadAllContacts()
    {
        // Wrap the query  inside []
        // Use collection to hold the data 
        List<Contact> conlst = [SELECT Id,Name, AccountId FROM Contact]; // One query
        
        System.debug('Value of conlst collection:: ' + conlst);
        
        System.debug('Total contacts in the  conlst collection is ' + conlst.size());
        
        // for loop - Version I

        // Looping data from the collection
        for(Integer index = 0; index < conlst.size(); index++)
        {
            System.debug('Contact Name ' + conlst[index].Name);
            System.debug('Contact Id ' + conlst[index].Id);
            System.debug('Account  Id ' + conlst[index].AccountId);           

        }
        

        // Looping data from the collection
        for(Contact con :conlst)
        {
            System.debug('Contact Name ' + con.Name);
            System.debug('Contact Id ' + con.Id);
            System.debug('Account  Id ' + con.AccountId);           

        }
        
        /*for(Legal_Review_Request__c	 objLegal : legalLst) // Referrencing custom object
        {
            
        }*/
        
    }
    
    public static void FindingDuplicateLead(String strLeadName, String strEmail, String strCompanyName)
    {
        /*
         1. Use Email as a unique identifier to find a lead is already there or not
         */
         
        // 1. Take Email as input
        // 2. Run a check in DB (using SOQL query) and see if there are one or more records have the same email
        // 3. If there is one or more records, throw a msg saying the record already exists
        // 4. Else, leave it as is and continue with the code execution

         // Wrap the query  inside []
        // Use collection to hold the data 
        List<Lead> leadlst = [SELECT Id,Name, Email FROM Lead WHERE Email =: strEmail];
        
        System.debug('Total leads in the Leadlst collection is ' + leadlst.size());
        
        if(leadlst.size() > 0)
        {
            System.debug('Error - Already lead with the same email exists !!!');
        }
      	else
        {
            System.debug('Dup check - No duplicate lead with the same email is found. Good to insert lead into the DB!!!');
            
            Lead objLead = new Lead(); // Step 1: Initalize 
            objLead.email = strEmail;
            objLead.LastName = strLeadName;
            objLead.Company = strCompanyName;
            insert objLead;
        }
    } 
    
     public static void DataRelationShipQueries()
     {
         List<Contact> conlst = [SELECT Id,Name, Email, AccountId, Account.Name FROM Contact];
         
         // Looping data from the collection
         for(Contact con :conlst)
         {
             System.debug('Contact Name ' + con.Name);
             System.debug('Contact Id ' + con.Id);
             System.debug('Account  Id ' + con.AccountId);   
             System.debug('Account  Name ' + con.Account.Name);
             
         }
         
     }
    
    
    public static void accountByState_simple(String state)
    {
        List<Account> acclist = [Select Id , Name from Account WHERE BillingState = :state];
        //System.debug('Value of acclist collection:: ' + acclist);
        
	}
    
    public static List<Account> accountByState(String state)
    {
        List<Account> acclist = [Select Id , Name from Account WHERE BillingState =:state];
        return acclist;
	}
    
    public static void SayHello_simple(string strName)
    {
        string strOutputMessage = 'Hi ' + strName;
    }
    
    public static String SayHello(string strName)
    {
        string strOutputMessage = 'Hi ' + strName;
        return strOutputMessage;
    }
    
    public static List<String> GetDays()
    {
        List<String> lstWeekDays = new List<String>();
      	lstWeekDays.add('Monday');
        lstWeekDays.add('Tuesday');
        lstWeekDays.add('Wednesday');
        lstWeekDays.add('Thursday');
        lstWeekDays.add('Friday');
        
       return lstWeekDays;
    }
    
    public static void GetDays_usingarray()
    {
        String[] weekdays = new List<String>();
      	weekdays.add('Monday');
        weekdays.add('Tuesday');
        weekdays.add('Wednesday');
        weekdays.add('Thursday');
        weekdays.add('Friday');
        
        System.debug('Value of weekdays:' + weekdays);

    }
    
    	
   

}